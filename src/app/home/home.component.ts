import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { DeviceSizeService } from '../providers/device-size.service';
import { DicoService } from '../language/dico.service';

import { AuthService } from '../auth/auth-service/auth.service';
import { HallsService, Hall } from '../halls/halls.service';


@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {

	public hallsList: Hall[];
	private unsubscribe: Subject<void> = new Subject();

  constructor(
    public auth: AuthService,
    public media: DeviceSizeService,
    public location: Location,
    public d: DicoService,
		public halls: HallsService
  ) { }

  ngOnInit() {
    this.media.start();
		this.auth.start();
		
		this.halls.getHalls()
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((hallsList) => {
			this.hallsList = hallsList;
		});
  }

  ngOnDestroy() {
    this.media.stop();
		this.auth.stop();
  }

	userIsAdmin(hallId: string): boolean {
		if (this.auth.getUserProfile() && this.auth.getUserProfile()['halls']) {
			return this.auth.getUserProfile()['halls'][hallId];
		}
		return false;
	}

}
