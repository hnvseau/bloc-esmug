import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {AuthService, Profile} from '../auth/auth-service/auth.service';
import {DicoService} from '../language/dico.service';
import {DeleteDialogComponent} from '../shared-components/delete-dialog/delete-dialog.component';
import {UpdatePasswordDialogComponent} from './components/update-password-dialog/update-password-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Location} from '@angular/common';

import { pipe } from 'rxjs';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  name: string;
	email: string;

  constructor(
    private auth: AuthService,
    public d: DicoService,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public location: Location
  ) { }

  ngOnInit() {
    this.auth.waitForProfileToBeSet().pipe(first()).subscribe(profile => {
			if (profile) {
				this.name = profile.name.firstName + " " + profile.name.lastName;
			}
			this.email = this.auth.getCurrentUser().email;
    });
  }

  newPassword() {
    this.dialog.open(UpdatePasswordDialogComponent)
    .afterClosed().subscribe(result => {
      if (result){
        this.auth.updatePassword(result.password)
        .then(() => {
          this.snackBar.open(this.d.l.changesApplied, 'ok', {duration: 2000});
          this.auth.setError(this.d.l.passwordChangedInfo, true);
          this.auth.logout();
        })
        .catch(() => {
          this.snackBar.open('erreur', 'ok', {duration: 2000});
          this.auth.setError(this.d.l.refreshTokenInfo, true);
          this.auth.goToLogin();
        })
      }
    });
  }
}
