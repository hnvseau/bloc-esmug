import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AuthService } from '../../auth/auth-service/auth.service';
import { pipe } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Injectable()
export class AccountGuard implements CanActivate {

  constructor(
    private auth: AuthService
  ) { }

  canActivate() {
    return this.auth.waitForProfileToBeSet().pipe(
			take(1),
			map(profile => {
				if (!this.auth.getCurrentUser()) {
					this.auth.goToLogin();
					return false
				} else {
					return true;
				}
			})
		);
  }
}
