import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './auth/auth-login/login.component';
import { PasswordResetComponent } from './auth/password-reset/password-reset.component';

// TODO: admin part
import { CanActivateAdmin } from './admin/guard/admin-guard.service';

import { AccountComponent } from './account/account.component';
import { AccountGuard } from './account/account-guard/account.guard';

import { InfoComponent } from './info/info.component';

import { HallsDashComponent } from './halls/halls-dash/halls-dash.component';
import { WallsDashComponent } from './walls/walls-dash/walls-dash.component';
import { EditWallComponent } from './walls/edit-wall/edit-wall.component';

import { BlocsDashComponent } from './blocs/blocs-dash/blocs-dash.component';
import { EditBlocComponent } from './blocs/edit-bloc/edit-bloc.component';
import { BlocDisplayComponent } from './blocs/bloc-display/bloc-display.component';

import { ImageBrowserComponent } from './shared-components/image-browser/image-browser.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },

  { path: 'login', component: LoginComponent },
  { path: 'infos', component: InfoComponent },
  { path: 'password_reset', component: PasswordResetComponent },

  { path: 'account', component: AccountComponent, canActivate: [AccountGuard] },

  { path: 'home', component: HallsDashComponent },
	{ path: 'walls/:hallId', component: WallsDashComponent },
	{ path: 'blocs/:hallId', component: BlocsDashComponent },
	{ path: 'holds/:hallId/:blocId', component: BlocDisplayComponent },

	{ path: 'walls/:hallId/:wallId/edit', component: EditWallComponent },
	{ path: 'blocs/:hallId/:blocId/edit', component: EditBlocComponent },
	{ path: 'image', component: ImageBrowserComponent }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' }) ],
  exports: [ RouterModule ]
 })
export class AppRoutingModule {  }
