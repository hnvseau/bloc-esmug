import { EventEmitter, Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { FormControl } from '@angular/forms';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';

import { ToolsService } from '../../providers/tools.service';
import { DicoService } from '../../language/dico.service';

import { environment } from '../../../environments/environment';
import { Observable } from '../../../../node_modules/rxjs';

export class Profile {
  name: {
    firstName: string,
    lastName: string
  };
  admin: boolean;
  email: string;
  halls: any;
}

@Injectable()
export class AuthService {

  error: any;
  error_persist: boolean;

  currentUser: any;
  userWatcher: any;
  userReady: boolean = false;

  profile: Profile;
  profileWatcher: any;
  profileListener = new EventEmitter();


  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router,
    public location: Location,
    private tools: ToolsService,
    public d: DicoService
  ) {
    this.currentUser = this.afAuth.currentUser;
    this.error_persist = false;
  }

  start() {
    this.userWatcher = this.watchUser();
  }

  stop() {
    if (this.userWatcher) this.userWatcher.unsubscribe();
  }

  watchUser() {
    return this.afAuth.onIdTokenChanged((user) => {
      if (this.profileWatcher) {
        this.profileWatcher.unsubscribe();
        this.profileWatcher = null;
        this.profile = null;
      }
      if (!user) {
        this.currentUser = null;
        this.userReady = true;
        this.profileListener.emit(true);
      } else {
        this.currentUser = user;
        this.profileWatcher = this.watchProfile();
        this.userReady = true;
      }
    });
  }

  watchProfile() {
    return this.db.object<Profile>('admins/' + this.getCurrentUser().uid)
    .valueChanges().subscribe(
      profile => {
        this.profile = profile;
        this.profileListener.emit(true);
      },
			error => {}
    );
  }

  logout() {
    this.afAuth.signOut();
  }

  login(email: string, password: string) {
    this.afAuth.signInWithEmailAndPassword(email, password)
    .then(
      (success) => { this.goToHome() },
      (err) => { this.onLoginError(err) }
    );
  }

  private onLoginError(error: any) {
    console.log(error);
    this.error = error;
  }

  updatePassword(password: string) {
    return this.getCurrentUser().updatePassword(password);
  }

  goToLogin() {
    this.router.navigateByUrl('/login');
  }

  goToHome() {
    this.router.navigateByUrl('/home');
  }

  setError(message: string, persist: boolean) {
    this.error = message;
    this.error_persist = persist;
  }

  resetError() {
    if (this.error_persist) {
      this.error_persist = false;
    } else {
      this.error = null;
    }
  }

  sendPasswordResetEmail(email: string) {
    this.afAuth.sendPasswordResetEmail(email)
    .catch((err) => { console.log(err) });
  }

  confirmPasswordReset(code: string, password: string) {
    this.afAuth.confirmPasswordReset(code, password)
    .catch((err) => { console.log(err) });
  }

  getCurrentUser() {
    return this.currentUser;
  }

  getUserProfile() {
    return this.profile;
  }

  waitForProfileToBeSet() {
    return new Observable<Profile>(observer => {
      if (this.profile || (this.userReady && !this.getCurrentUser())) {
        observer.next(this.profile);
        observer.complete();
      } else {
        this.profileListener.subscribe(ok => {
          if (ok) {
            observer.next(this.profile);
            observer.complete();
          }
        });
      }
    });
  }

}
