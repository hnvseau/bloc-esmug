import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';

import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { DicoService } from '../../language/dico.service';

import { HoldsListComponent } from '../../holds/holds-list/holds-list.component';
import { HoldsService, WallHold } from '../../holds/holds.service';
import { WallsService, Wall } from '../walls.service';

@Component({
  selector: 'app-edit-wall',
  templateUrl: './edit-wall.component.html',
  styleUrls: ['./edit-wall.component.css']
})
export class EditWallComponent implements OnInit, OnDestroy {

	public hallId: string;
	public wallId: string;
	public image: string;
	public new: boolean = false;
  public formGroup: FormGroup;
  private unsubscribe: Subject<void> = new Subject();

	@ViewChild("holdsCmp", { read: HoldsListComponent }) holdsCmp: HoldsListComponent;

  constructor(
		public location: Location,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private d: DicoService,
		private holds: HoldsService,
		private walls: WallsService
	) { }

  ngOnInit() {
    this.hallId = this.route.snapshot.paramMap.get('hallId');
    this.wallId = this.route.snapshot.paramMap.get('wallId');
		if (this.wallId == "-") {
			this.new = true;
		}
    this.initFormGroup();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

	submit() {
		this.walls.editWall(this.hallId, {
			wallId: this.wallId,
      name: this.formGroup.get('name').value,
      // description: this.formGroup.get('description').value,
			image: this.image,
			setUpDate: this.formGroup.get('setUpDate').value,
			circleRadius: this.formGroup.get('circleRadius').value,
			circleWidth: this.formGroup.get('circleWidth').value
		}, this.holdsCmp.getWallHolds()).then(() => {
      this.snackBar.open(this.d.l.changesApplied, 'ok', {duration: 2000});
      this.location.back();
    }).catch(reason => {
      this.snackBar.open(reason, 'ok', {duration: 2000});
    });
	}

  initFormGroup() {
    this.walls.getWall(this.hallId, this.wallId).pipe(takeUntil(this.unsubscribe))
    .subscribe((wall) => {
      if (!wall) {
        wall = new Wall();
        this.wallId = this.walls.getNewWallId(this.hallId);
      }
      this.formGroup = this.fb.group({
        name: [wall.name || '', [Validators.required, Validators.maxLength(50)]],
				setUpDate: [wall.setUpDate || '', [Validators.required]],
				circleRadius: [wall.circleRadius || 100, [Validators.required]],
				circleWidth: [wall.circleWidth || 100, [Validators.required]]
        // description: [wall.description || '', [Validators.maxLength(2000)]]
      });
			this.image = wall.image;
    });
  }
}
