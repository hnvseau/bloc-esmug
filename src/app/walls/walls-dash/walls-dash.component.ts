import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeleteDialogComponent } from '../../shared-components/delete-dialog/delete-dialog.component';

import { AuthService } from '../../auth/auth-service/auth.service';
import { WallsService, Wall } from '../walls.service';
import { HallsService, Hall } from '../../halls/halls.service';

@Component({
  selector: 'app-walls-dash',
  templateUrl: './walls-dash.component.html',
  styleUrls: ['./walls-dash.component.css']
})
export class WallsDashComponent implements OnInit {

	public wallsList: Wall[];
	public hall: Hall;
  private unsubscribe: Subject<void> = new Subject();

	@Input() hallId: string;
	@Input() select: boolean = false;
	@Output() wallId: EventEmitter<string> = new EventEmitter<string>();

  constructor(
		private dialog: MatDialog,
		private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    public location: Location,
		private auth: AuthService,
		public walls: WallsService,
		public halls: HallsService
	) { }

  ngOnInit() {
		if (!this.select) {
			this.hallId = this.route.snapshot.paramMap.get('hallId');
		}
		
		this.halls.getHall(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((hall) => {
			this.hall = hall;
		});

		this.walls.getWalls(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe(walls => {
			this.wallsList = walls;
		})
  }

	userIsAdmin(): boolean {
		if (this.auth.getUserProfile() && this.auth.getUserProfile()['halls']) {
			return this.auth.getUserProfile()['halls'][this.hallId];
		}
		return false;
	}

  delete(wall: Wall) {
    this.dialog.open(DeleteDialogComponent, {
      data: {
        title: 'Confirmation de la suppression',
        content: `Êtes-vous certain de vouloir supprimer le pan de "${wall.name}" ?`
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        this.walls.removeWall(this.hallId, wall.wallId).then(() => {
					this.snackBar.open('Pan supprimé', 'ok', {duration: 2000});
					this.location.back();
				});
      }
    });
  }

}
