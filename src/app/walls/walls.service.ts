import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, pipe } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

import { BlocsService } from '../blocs/blocs.service';
import { HoldsService, WallHold } from '../holds/holds.service';
import { ToolsService } from '../providers/tools.service';

export class Wall {
  wallId: string;
  name: string;
  image: string;
  setUpDate: string;
	circleRadius: number;
	circleWidth: number;
}

@Injectable()
export class WallsService {

	walls: { [hallId: string]: Observable<Wall[]> } = {};
	wallsList: { [hallId: string]: { [wallId: string]: Observable<Wall> } } = {};

  constructor(
		private db: AngularFireDatabase,
		private blocs: BlocsService,
		private holds: HoldsService,
		private tools: ToolsService
	) { }


	getWall(hallId: string, wallId: string): Observable<Wall> {
		if (!this.wallsList[hallId]) {
			this.wallsList[hallId] = {};
		}
		if (!this.wallsList[hallId][wallId]) {
			this.wallsList[hallId][wallId] = this.getWalls(hallId).pipe(
				map(walls => walls.find(wall => wall.wallId == wallId)),
				shareReplay(1)
			);
		}
		return this.wallsList[hallId][wallId];
	}

	getWalls(hallId: string): Observable<Wall[]> {
		if (!this.walls[hallId]) {
			this.walls[hallId] = this.tools.enableCache(
				this.db.list<Wall>('walls/' + hallId).valueChanges(),
				`ẁalls-${hallId}`
			).pipe(
				shareReplay(1)
			);
		}
		return this.walls[hallId];
	}

	getNewWallId(hallId: string) {
		return this.db.list<Wall>('walls/' + hallId).push(null).key;
	}

	editWall(hallId: string, wall: Wall, holds: WallHold[]) {
		return this.db.object<Wall>(
			'walls/' + hallId + '/' + wall.wallId
		).set(wall).then(() => {
			return this.holds.setWallHolds(hallId, wall.wallId, holds)
		});
	}

	removeWall(hallId: string, wallId: string) {
		return this.blocs.removeBlocs(hallId, wallId).then(() => {
			return this.db.object<Wall>('walls/' + hallId + '/' + wallId).remove();
		});
	}

}
