import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AuthService } from '../../auth/auth-service/auth.service';
import { pipe } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';

@Injectable()
export class CanActivateAdmin implements CanActivate {

  constructor(
    private auth: AuthService
  ) { }

  canActivate() {
    return this.auth.waitForProfileToBeSet().pipe(
			take(1),
			map(profile => profile.admin),
			tap(admin => {
	      if (!admin) {
	        this.auth.goToHome();
	        return;
	      }
	    })
		);
  }
}
