import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ToolsService } from '../../providers/tools.service';
import {Subscription} from '../../../../node_modules/rxjs';

@Injectable()
export class AdminService {

  users: any;

  public usersWatcher: Subscription;

  constructor(
    private db: AngularFireDatabase,
    private tools: ToolsService
  ) { }

}
