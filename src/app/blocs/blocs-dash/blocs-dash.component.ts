import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeleteDialogComponent } from '../../shared-components/delete-dialog/delete-dialog.component';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DicoService } from '../../language/dico.service';

import { AuthService } from '../../auth/auth-service/auth.service';
import { BlocsService, Bloc } from '../blocs.service';
import { HallsService, Hall } from '../../halls/halls.service';

@Component({
	selector: 'app-blocs-dash',
	templateUrl: './blocs-dash.component.html',
	styleUrls: ['./blocs-dash.component.css']
})
export class BlocsDashComponent implements OnInit {

	public blocsList: Bloc[];
	public displayedBlocsList: Bloc[];
	public hall: Hall;
	public hallId: string;
	public wallId: string;
	public formGroup: FormGroup;
	public showFilter: boolean = false;
	private unsubscribe: Subject<void> = new Subject();

	constructor(
		public location: Location,
		private auth: AuthService,
		private dialog: MatDialog,
		private snackBar: MatSnackBar,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private d: DicoService,
		private blocs: BlocsService,
		private halls: HallsService
	) { }

	ngOnInit() {
		this.hallId = this.route.snapshot.paramMap.get('hallId');

		this.halls.getHall(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((hall) => {
			this.hall = hall;
		});

		this.blocs.getBlocs(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe(blocsList => {
			this.blocsList = blocsList;
			this.filter();
		});

		this.formGroup = this.fb.group({
			minDiff: [24, []],
			maxDiff: [58, []],
			type0: [true, []],
			type1: [true, []],
			type2: [true, []],
		});
		this.formGroup.valueChanges.pipe(
			takeUntil(this.unsubscribe)
		).subscribe(() => { this.filter(); });
	}

	filter() {
		if (this.formGroup) {
			this.displayedBlocsList = this.blocsList.filter((bloc) => {
				let matchType = false;
				let matchDiff = false;
				if (this.formGroup.get('type' + bloc.type).value) {
					matchType = true;
				}
				if (this.formGroup.get('minDiff').value <= bloc.diff
						&& this.formGroup.get('maxDiff').value >= bloc.diff
				) {
					matchDiff = true;
				}
				return matchType && matchDiff;
			})
		} else {
			this.displayedBlocsList = this.blocsList;
		}
	}

	delete(bloc: Bloc) {
		this.dialog.open(DeleteDialogComponent, {
			data: {
				title: 'Confirmation de la suppression',
				content: `Êtes-vous certain de vouloir supprimer le bloc de "${bloc.author}" ?`
			}
		}).afterClosed().subscribe(result => {
			if (result) {
				this.blocs.removeBloc(this.hallId, bloc.blocId).then(() => {
					this.snackBar.open('Bloc supprimé', 'ok', {duration: 2000});
					this.location.back();
				});
			}
		});
	}

	userIsAdmin(): boolean {
		if (this.auth.getUserProfile() && this.auth.getUserProfile()['halls']) {
			return this.auth.getUserProfile()['halls'][this.hallId];
		}
		return false;
	}

}
