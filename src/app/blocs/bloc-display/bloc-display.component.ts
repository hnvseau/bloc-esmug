import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { DicoService } from '../../language/dico.service';
import { HoldsListComponent } from '../../holds/holds-list/holds-list.component';
import { HoldsService, BlocWall } from '../../holds/holds.service';
import { BlocsService, Bloc } from '../blocs.service';
import { WallsService, Wall } from '../../walls/walls.service';
import { HallsService, Hall } from '../../halls/halls.service';

@Component({
  selector: 'app-bloc-display',
  templateUrl: './bloc-display.component.html',
  styleUrls: ['./bloc-display.component.css']
})
export class BlocDisplayComponent implements OnInit, OnDestroy {

	public hallId: string;
	public blocId: string;
	public bloc: Bloc;
	public hall: Hall;
	public wallsList: { [wallId: string]: Wall } = {};
	public blocWalls: BlocWall[];
  private unsubscribe: Subject<void> = new Subject();

	private newRate: number = 0;
	private _rate: number;

	public isRate: boolean = false;
	public rated: boolean = false;
	public evaluated: boolean = false;

	set rate(value: number) {
		this.isRate = true;
		this._rate = value;
	}

	get rate() {
		return this._rate;
	}

  constructor(
		public location: Location,
    private route: ActivatedRoute,
		private d: DicoService,
		private holds: HoldsService,
		private blocs: BlocsService,
		private walls: WallsService,
		private halls: HallsService
	) { }

  ngOnInit() {
    this.hallId = this.route.snapshot.paramMap.get('hallId');
    this.blocId = this.route.snapshot.paramMap.get('blocId');

		this.halls.getHall(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((hall) => {
			this.hall = hall;
		});

		this.walls.getWalls(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((walls) => {
			walls.forEach((wall) => {
				this.wallsList[wall.wallId] = wall;
			});
		});

		this.blocs.getBloc(this.hallId, this.blocId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((bloc) => {
			this.bloc = bloc;
		});

		this.holds.getBlocWalls(this.hallId, this.blocId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((walls) => {
			this.blocWalls = walls;
		});
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

	evalDiff(value: number) {
		this.blocs.rateDiff(this.hallId, this.bloc, value);
		this.evaluated = true;
	}

	validateRate() {
		this.blocs.rateBloc(this.hallId, this.bloc, this._rate);
		this.rated = true;
	}


}
