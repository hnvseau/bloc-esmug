import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';

import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { DicoService } from '../../language/dico.service';

import { HoldsListComponent } from '../../holds/holds-list/holds-list.component';
import { HoldsService, BlocHold, BlocWall } from '../../holds/holds.service';
import { BlocsService, Bloc } from '../blocs.service';
import { WallsService, Wall } from '../../walls/walls.service';
import { HallsService, Hall } from '../../halls/halls.service';

@Component({
  selector: 'app-edit-bloc',
  templateUrl: './edit-bloc.component.html',
  styleUrls: ['./edit-bloc.component.css']
})
export class EditBlocComponent implements OnInit {

	public hallId: string;
	private blocId: string;
	public hall: Hall;
	public blocWalls: BlocWall[] = [];
	public wallImages: { [wallId: string]: Wall } = {};
	public diff: number = 24;
  public formGroup: FormGroup;
	public selectWall: boolean = false;
  private unsubscribe: Subject<void> = new Subject();

  constructor(
		public location: Location,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private d: DicoService,
		private holds: HoldsService,
		private blocs: BlocsService,
		private walls: WallsService,
		private halls: HallsService
	) { }

  ngOnInit() {
    this.hallId = this.route.snapshot.paramMap.get('hallId');
    this.blocId = this.route.snapshot.paramMap.get('blocId');
    this.initFormGroup();

		this.halls.getHall(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((hall) => {
			this.hall = hall;
		});

		this.walls.getWalls(this.hallId)
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((walls) => {
			walls.forEach((wall: Wall) => {
				this.walls[wall.wallId] = wall;
			})
		});
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

	changeDiff(value: number) {
		this.diff += value;
		if (this.diff > 58) {
			this.diff = 58;
		}
		if (this.diff < 24) {
			this.diff = 24;
		}
	}

	submit() {
		let wallIdList = {};
		for (let wall of this.blocWalls) {
			wallIdList[wall.wallId] = wall.wallId;
		}
		this.blocs.editBloc(this.hallId, {
			blocId: this.blocId,
			description: this.formGroup.get('description').value,
			author: this.formGroup.get('author').value,
			date: (new Date()).toString(),
			type: parseInt(this.formGroup.get('type').value),
			rate: 0,
			nbOfVotes: 0,
			diff: this.diff,
			diffVotes: 0,
			diffSum: 0,
			wallIdList: wallIdList
		}, this.blocWalls).then(() => {
      this.snackBar.open(this.d.l.changesApplied, 'ok', {duration: 2000});
			this.location.back();
    }).catch(reason => {
      this.snackBar.open(reason, 'ok', {duration: 2000});
    });
	}

  initFormGroup() {
    this.blocs.getBloc(this.hallId, this.blocId)
		.pipe(takeUntil(this.unsubscribe))
    .subscribe((bloc) => {
      if (!bloc) {
        bloc = new Bloc();
        this.blocId = this.blocs.getNewBlocId(this.hallId);
      }
      this.formGroup = this.fb.group({
				author: [bloc.author || '', [Validators.required, Validators.maxLength(30)]],
				type: [bloc.type || 0, [Validators.required, Validators.max(2), Validators.min(0)]],
				description: [bloc.description || "", [Validators.maxLength(512)]]
      });
    });
  }

	setHolds(index: number, blocHolds: BlocHold[]) {
		this.blocWalls[index].holds = blocHolds;
	}

	add(wallId: string) {
		this.blocWalls.push(new BlocWall(wallId, this.blocWalls.length));
		this.selectWall = false;
	}

	remove() {
		this.blocWalls.pop();
	}

	invalid() {
		if (this.blocWalls.length < 1) {
			return true;
		}
		for (let wall of this.blocWalls) {
			if (wall.holds.length < 3) {
				return true;
			}
		}
		return false;
	}
}
