import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, pipe } from 'rxjs';
import { map, first, shareReplay } from 'rxjs/operators';

import { HoldsService, BlocWall } from '../holds/holds.service';

export class Bloc {
  blocId: string;
	description: string;
  author: string;
  date: string;
	type: number;
	rate: number;
	nbOfVotes: number;
	diff: number;
	diffVotes: number;
	diffSum: number;
	wallIdList: { [wallId: string]: string }
}

@Injectable()
export class BlocsService {

	blocs: { [hallId: string]: Observable<Bloc[]> } = {};
	blocsList: { [hallId: string]: { [blocId: string]: Observable<Bloc> } } = {};

  constructor(
		private db: AngularFireDatabase,
		private holds: HoldsService
	) { }

	getBloc(hallId: string, blocId: string): Observable<Bloc> {
		// if (this.blocs[hallId]) {
		// 	return this.blocs[hallId].map(blocs => {
		// 		for (let bloc of blocs) {
		// 			if (bloc.blocId == blocId) {
		// 				return bloc;
		// 			}
		// 		}
		// 	})
		// }
		if (!this.blocsList[hallId]) {
			this.blocsList[hallId] = {};
		}
		if (!this.blocsList[hallId][blocId]) {
			this.blocsList[hallId][blocId] = this.db.object<Bloc>(
				'blocs/' + hallId + '/' + '/' + blocId
			).valueChanges().pipe(
				shareReplay(1)
			);
		}
		return this.blocsList[hallId][blocId];
	}

	getBlocs(hallId: string): Observable<Bloc[]> {
		if (!this.blocs[hallId]) {
			this.blocs[hallId] = this.db.list<Bloc>(
				'blocs/' + hallId, ref => ref.orderByChild('rate')
			).valueChanges().pipe(
				map(blocs => blocs.reverse()),
				shareReplay(1)
			)
		}
		return this.blocs[hallId];
	}

	getNewBlocId(hallId: string) {
		return this.db.list<Bloc>('blocs/' + hallId).push(null).key;
	}

	editBloc(hallId: string, bloc: Bloc, walls: BlocWall[]) {
		return this.db.object<Bloc>('blocs/' + hallId + '/' + bloc.blocId)
		.set(bloc).then(() => {
			return this.holds.setBlocWalls(hallId, bloc.blocId, walls)
		})
	}

	rateBloc(hallId: string, bloc: Bloc, value: number) {
		let refs = {};
		refs['rate'] = (value + bloc.rate * bloc.nbOfVotes) / (bloc.nbOfVotes + 1);
		refs['nbOfVotes'] = bloc.nbOfVotes + 1;
		return this.db.object<any>('blocs/' + hallId + '/' + bloc.blocId).update(refs);
	}

	rateDiff(hallId: string, bloc: Bloc, value: number) {
		let refs = {};
		refs['diffVotes'] = bloc.diffVotes + 1;
		refs['diffSum'] = bloc.diffSum + value;
		if (refs['diffVotes'] > 10 && Math.abs(refs['diffSum'] / refs['diffVotes']) > 3/4) {
			refs['diff'] = bloc.diff + Math.sign(refs['diffSum']);
			refs['diffVotes'] = 0;
			refs['diffSum'] = 0;
		}
		return this.db.object<any>('blocs/' + hallId + '/' + bloc.blocId).update(refs);
	}

	removeBloc(hallId: string, blocId: string) {
		return this.holds.removeBloc(hallId, blocId).then(() => {
			return this.db.object<Bloc>('blocs/' + hallId + '/' + blocId).remove();
		});
	}

	removeBlocs(hallId: string, wallId: string) {
		return this.getBlocs(hallId)
    .pipe(first())
    .toPromise()
		.then(
			blocs => {
				let delBlocs = blocs.filter(bloc => bloc.wallIdList[wallId] == wallId).map(bloc => bloc.blocId);
				return this.holds.removeBlocs(hallId, delBlocs).then(() => {
					let refs = {}
					for (let bloc of blocs) {
						if (bloc.wallIdList[wallId] == wallId) {
							refs[bloc.blocId] = null;
						}
					}
					return this.db.object<any>('blocs/' + hallId).update(refs);
				});
			}
		);
	}

	computeDiff(value: number) {
		return "  " + Math.floor(value/6) + ["a", "a+ ", "b", "b+ ", "c", "c+"][value % 6];
	}

	getType(value: number) {
		switch (value) {
			case 0:
				return "Bloc";
			case 1:
				return "Circuit";
			case 2:
				return "Boucle";
		}
	}
}
