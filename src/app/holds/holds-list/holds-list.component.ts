import { Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { HoldsService, BlocHold, WallHold } from '../holds.service';

const CANVAS_WIDTH = 2000;
const CANVAS_HEIGHT = 1400
const CIRCLE_RADIUS = CANVAS_WIDTH / 100;
const LINE_WIDTH = CANVAS_WIDTH / 1000;
const MAX_TYPE = 2;

@Component({
  selector: 'app-holds-list',
  templateUrl: './holds-list.component.html',
  styleUrls: ['./holds-list.component.css']
})
export class HoldsListComponent implements OnInit {

	public wallHolds: WallHold[];
	public blocHolds: BlocHold[];

	public isImageLoading: boolean = true;
	public imgElement: any;

	private unsubscribe: Subject<void> = new Subject();

	private _circleRadius: number;
	private _circleWidth: number;

	@Input() hallId: string;
	@Input() wallId: string;

	@Input() set blocs(blocHolds: BlocHold[]) {
		this.blocHolds = blocHolds || [];
		if (!this.isImageLoading) {
			this.drawBlocHolds();
		}
	};
	@Input() set image(dataURL: string) {
		this.loadWall(dataURL);
	};

	@Input() set circleRadius(circleRadius: number) {
		this._circleRadius = circleRadius || 100;
		if (!this.isImageLoading) {
			this.drawBlocHolds();
			this.drawWallHolds();
		}
	};
	@Input() set circleWidth(circleWidth: number) {
		this._circleWidth = circleWidth || 100;
		if (!this.isImageLoading) {
			this.drawBlocHolds();
			this.drawWallHolds();
		}
	};

	@Input() state: string = "def";

	@Output() holds: EventEmitter<BlocHold[]> = new EventEmitter<BlocHold[]>();

	@ViewChild("canvas", { read: ElementRef, static: true }) canvas: ElementRef;

  constructor(
		private sz: DomSanitizer,
    private http: HttpClient,
		private hs: HoldsService
	) { }

  ngOnInit() {
		if (this.state === "def" || this.state === "sel") {
			if (!this.blocHolds) {
				this.blocHolds = [];
			}
			this.hs.getWallHolds(this.hallId, this.wallId)
			.pipe(takeUntil(this.unsubscribe))
			.subscribe(holds => {
				this.wallHolds = holds || []
				this.drawWallHolds();
			});
		}
  }

	getWallHolds(): WallHold[] {
		return this.wallHolds;
	}
	getBlocHolds(): BlocHold[] {
		return this.blocHolds;
	}

	onImageClick(event: MouseEvent) {
		if (this.isImageLoading) {
			return;
		}
		var rect = this.canvas.nativeElement.getBoundingClientRect();
		var xCoord = event.clientX - rect.left;
		var yCoord = event.clientY - rect.top;
		this.onClick(
			(event.clientX - rect.left) / this.getScale(),
			(event.clientY - rect.top) / this.getScale()
		);
	}

	// normalized coodinates 0 <= x,y <= 1
	onClick(xCoord: number, yCoord: number) {
		if (this.state === "def") {
			this.onDefClick(xCoord, yCoord);
		} else if (this.state === "sel") {
			this.onSelClick(xCoord, yCoord);
			this.holds.emit(this.blocHolds);
		}
	}

	onDefClick(xCoord: number, yCoord: number) {
		let newList = [];
		let index = 0;
		let found = false;
		for (let hold of this.wallHolds) {
			if (this.dist(xCoord, yCoord, hold.xCoord, hold.yCoord) < this.getCircleRadius()) {
				found = true
			} else {
				hold.holdId = '' + index;
				newList.push(hold);
				index ++;
			}
		}
		if (!found) {
			newList.push(new WallHold('' + index, xCoord, yCoord));
		}
		this.wallHolds = newList;
		this.drawWallHolds();
	}

	onSelClick(xCoord: number, yCoord: number) {
		for (let hold of this.wallHolds) {
			if (this.dist(xCoord, yCoord, hold.xCoord, hold.yCoord) < this.getCircleRadius()) {
				let newList = [];
				let index = 0;
				let found = false;
				for (let bhold of this.blocHolds) {
					if (bhold.holdId === hold.holdId) {
						found = true;
						if (bhold.type==MAX_TYPE) {
							continue;
						} else {
							bhold.type += 1;
						}
					}
					bhold.index = index;
					newList.push(bhold);
					index ++;
				}
				if (!found) {
					newList.push(new BlocHold(hold.holdId, index, hold.xCoord, hold.yCoord));
				}
				this.blocHolds = newList;
				this.drawBlocHolds();
				break;
			}
		}
	}

	dist(x1: number, y1: number, x2: number, y2: number) {
		return Math.sqrt((x2 - x1)**2 + (y2 - y1)**2);
	}

	getScale() {
		var rect = this.canvas.nativeElement.getBoundingClientRect();
		return (rect.right-rect.left) / this.getCanvasWidth();
	}

	getCanvasWidth() {
		if (this.imgElement) {
			return this.canvas.nativeElement.width;
		} else {
			return CANVAS_WIDTH;
		}
	}

	getCanvasHeight() {
		if (this.imgElement) {
			return this.canvas.nativeElement.height;
		} else {
			return CANVAS_HEIGHT;
		}
	}

	getCircleRadius() {
		if (this.imgElement) {
			return (this._circleRadius / 100) * (this.getCanvasWidth() / 100);
		} else {
			return CIRCLE_RADIUS;
		}
	}

	getLineWidth() {
		if (this.imgElement) {
			return (this._circleWidth / 100) * (this.getCanvasWidth() / 1000);
		} else {
			return LINE_WIDTH;
		}
	}

	drawHold(xCoord: number, yCoord: number, index:number=null, type:number=0) {
		var ctx = this.canvas.nativeElement.getContext("2d");

		if (this.state === "def") {
			ctx.strokeStyle="#FF0000";
		} else {
			switch (type) {
				case 0:
					ctx.strokeStyle="#FF0000";
					break;
				case 1:
					ctx.strokeStyle="#00FF00";
					break;
				case 2:
					ctx.strokeStyle="#0000FF";
					break;
			}
			// TODO: draw number
		}

		ctx.lineWidth = this.getLineWidth();
		ctx.beginPath();
		ctx.arc(
			xCoord,
			yCoord,
			this.getCircleRadius(),
			0, 2*Math.PI
		);
		ctx.stroke();
	}

	drawWallHolds() {
		this.resetWall();
		if (!this.isImageLoading) {
			this.wallHolds.forEach(hold => {
				this.drawHold(hold.xCoord, hold.yCoord);
			});
		}
	}

	drawBlocHolds() {
		this.resetWall();
		if (!this.isImageLoading) {
			this.blocHolds.forEach(hold => {
				this.drawHold(hold.xCoord, hold.yCoord, hold.index, hold.type);
			});
		}
	}

	loadWall(image) {
		if (!image) {
			return;
		}
		var ctx = this.canvas.nativeElement.getContext("2d");
		this.imgElement = new Image();
		this.isImageLoading = true;

		// reset holds definition
		if (this.state === "sel") {
			this.wallHolds = [];
		}

		// draw on loading completed
		this.imgElement.addEventListener('load', () => {
			this.canvas.nativeElement.width = this.imgElement.width;
			this.canvas.nativeElement.height = this.imgElement.height;
			this.isImageLoading = false;
			if ((this.state === "sel" || this.state === "disp") && this.blocHolds) {
				this.drawBlocHolds();
			} else if (this.state === "def" && this.wallHolds) {
				this.drawWallHolds();
			} else {
				this.drawWall();
			}
		}, false);
		this.imgElement.src = image;
	}

	drawWall() {
		if (this.imgElement && !this.isImageLoading) {
			var ctx = this.canvas.nativeElement.getContext("2d");
			ctx.drawImage(
				this.imgElement, 0, 0,
				this.getCanvasWidth(),
				this.getCanvasHeight()
			);
		}
	}

	resetWall() {
		this.drawWall();
	}

}
