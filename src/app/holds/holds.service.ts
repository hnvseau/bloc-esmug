import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, pipe } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

export class Hold {
	holdId: string;
	xCoord: number;
	yCoord: number;
}

export class BlocHold extends Hold {
	type: number;
	index: number;

	constructor(holdId: string, index: number, xCoord: number, yCoord: number) {
		super();
		this.holdId = holdId;
		this.index = index;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.type = 0;
	}
}

export class WallHold extends Hold {
	constructor(holdId: string, xCoord: number, yCoord: number) {
		super();
		this.holdId = holdId;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}
}

export class BlocWall {
	wallId: string;
	index: number;
	holds: BlocHold[];

	constructor(wallId: string, index: number, holds: BlocHold[] = []) {
		this.wallId = wallId;
		this.index = index;
		this.holds = holds;
	}
}

@Injectable()
export class HoldsService {

	wallHoldsList: { [hallId: string]: { [wallId: string]: Observable<WallHold[]> } } = {};
	blocWallsList: { [hallId: string]: { [blocId: string]: Observable<BlocWall[]> } } = {};

  constructor(
		private db: AngularFireDatabase
	) { }

	getBlocWalls(hallId: string, blocId: string): Observable<BlocWall[]> {
		if (!this.blocWallsList[hallId]) {
			this.blocWallsList[hallId] = {};
		}
		if (!this.blocWallsList[hallId][blocId]) {
			this.blocWallsList[hallId][blocId] = this.db.list<BlocWall>('holds/' + hallId + '/blocs/' + blocId)
			.valueChanges().pipe(shareReplay(1));
		}
		return this.blocWallsList[hallId][blocId];
	}

	setBlocWalls(hallId: string, blocId: string, walls: BlocWall[]) {
		let list = {};
		walls.forEach(wall => {
			list[wall.index] = wall;
		});
		return this.db.object<any>('holds/' + hallId + '/blocs/' + blocId)
		.set(list);
	}

	removeBloc(hallId: string, blocId: string) {
		return this.db.object<any>('holds/' + hallId + '/blocs/' + blocId)
		.remove();
	}

	removeBlocs(hallId: string, blocIdList: string[]) {
		let refs = {};
		for (let blocId of blocIdList) {
			refs[blocId] = null;
		}
		return this.db.object<any>('holds/' + hallId + '/blocs').update(refs);
	}

	getWallHolds(hallId: string, wallId: string): Observable<WallHold[]> {
		if (!this.wallHoldsList[hallId]) {
			this.wallHoldsList[hallId] = {};
		}
		if (!this.wallHoldsList[hallId][wallId]) {
			this.wallHoldsList[hallId][wallId] = this.db.list<WallHold>('holds/' + hallId + '/walls/' + wallId)
			.valueChanges().pipe(shareReplay(1));
		}
		return this.wallHoldsList[hallId][wallId];
	}

	setWallHolds(hallId: string, wallId: string, holds: WallHold[]) {
		let list = {};
		holds.forEach(hold => {
			list[hold.holdId] = hold;
		});
		return this.db.object<any>('holds/' + hallId + '/walls/' + wallId)
		.set(list);
	}
}
