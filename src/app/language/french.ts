import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

export class French {
  // Common
  public static readonly appName = "U-Bloc";
  public static readonly backLabel = "Retour";
  public static readonly confirmLabel = "Confimer";
  public static readonly cancelLabel = "Annuler";
  public static readonly modifyLabel = "Modifier";
  public static readonly applyChangesLabel = "Appliquer";
  public static readonly okLabel = "Ok";
  public static readonly changesApplied = "Changements appliqués avec succès";
  public static readonly search = "recherche";

  public static readonly locale = "fr";
  public static readonly timeFormat = "H:mm";
  public static readonly shortDateFormat = "E d LLL y";
  public static readonly longDateFormat = "EEEE d LLLL y";
  public static readonly shortDateTimeFormat = "E d LLL y H:mm";
  public static readonly longDateTimeFormat = "EEEE d LLLL y H:mm";

  // Auth
  public static readonly logOutLabel = "Déconnexion";
  public static readonly signInPageTitle = "Se connecter en tant qu'administrateur";
  public static readonly signInButtonLabel = "Connexion";
  public static readonly passwordlLabel = "Mot de passe";
  public static readonly askForExistingPwdLabel = "Entre ton mot de passe";
  public static readonly emailLabel = "E-mail";
  public static readonly askForExistingEmailLabel = "Entre ton e-mail";
  public static readonly noEmailError = "L'e-mail est exigé";
  public static readonly emailFormatError = "L'e-mail est incorrect";
  public static readonly beforePwdRstEmailInfo = "Un e-mail de réinitialisation de mot de passe va être envoyé à l'adresse donnée";
  public static readonly sendPwdResetEmailLabel = "Envoyer l'e-mail";
  public static readonly afterPwdRstEmailInfo = "Ouvre le lien contenu dans l'email envoyé à {0}";
  public static readonly passwordTooShortError = "Mot de passe trop court";
  public static readonly toSignInLabel = "Crée-toi un compte !";
  public static readonly passwordForgottenLabel = "Mot de passe oublié ?";
  public static readonly accountPageTitle = "Informations personnelles";
  public static readonly refreshTokenInfo = "Cette opréation nécéssite de s'être identifié recemment";
  public static readonly passwordChangedInfo = "Ton mot de passe a été modifié avec succès";
  public static readonly changePasswordButtonLabel = "Changer le mot de passe";

  // Info
  public static readonly legalNoticeLabel = "Mentions légales";
  public static readonly userGuideLabel = "Détail des fonctionnalités";

}
