import {
	Component, OnInit,
	ViewChild, ElementRef,
	EventEmitter, Input, Output
} from '@angular/core';

@Component({
  selector: 'app-image-browser',
  templateUrl: './image-browser.component.html',
  styleUrls: ['./image-browser.component.css']
})
export class ImageBrowserComponent implements OnInit {

	dataURL: string;
	@Input() preview: boolean = false;
	@Output() imageOut: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

	openFile(file) {
		var input = file.target;
		var reader = new FileReader();
		reader.onload = function(){
		};
		reader.addEventListener("load", () => {
			this.dataURL = reader.result as string;
			this.imageOut.emit(this.dataURL);
		}, false);
    if (input.files) {
      reader.readAsDataURL(input.files[0]);
    }
	}

}
