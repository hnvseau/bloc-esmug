// angular modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';
// import { NgvasModule } from 'ngvas';

// angularfire modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';

// components
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/auth-login/login.component';
import { PasswordResetComponent } from './auth/password-reset/password-reset.component';

// app modules
import { AccountComponent } from './account/account.component';

import { InfoComponent } from './info/info.component';
import { LegalNoticeComponent } from './info/legal-notice/legal-notice.component';
import { ReadmeComponent } from './info/readme/readme.component';

import { HallsDashComponent } from './halls/halls-dash/halls-dash.component';

import { WallsDashComponent } from './walls/walls-dash/walls-dash.component';
import { EditWallComponent } from './walls/edit-wall/edit-wall.component';

import { BlocsDashComponent } from './blocs/blocs-dash/blocs-dash.component';
import { EditBlocComponent } from './blocs/edit-bloc/edit-bloc.component';

import { HoldsListComponent } from './holds/holds-list/holds-list.component';

// services
import { AuthService } from './auth/auth-service/auth.service';
import { AdminService } from './admin/admin-service/admin.service';

import { DeviceSizeService } from './providers/device-size.service';
import { ToolsService } from './providers/tools.service';
import { DicoService } from './language/dico.service';

import { HallsService } from './halls/halls.service';
import { WallsService } from './walls/walls.service';
import { BlocsService } from './blocs/blocs.service';
import { HoldsService } from './holds/holds.service';

// Guards
import { CanActivateAdmin } from './admin/guard/admin-guard.service';
import { AccountGuard } from './account/account-guard/account.guard';


// angular materials
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';

import { environment } from '../environments/environment';

import { DeleteDialogComponent } from './shared-components/delete-dialog/delete-dialog.component';
import { LoginDialogComponent } from './shared-components/login-dialog/login-dialog.component';
import { PagedGridComponent } from './shared-components/paged-grid/paged-grid.component';
import { LoadingComponent } from './shared-components/loading/loading.component';
import { ConditionalRouterLinkComponent } from './shared-components/conditional-router-link/conditional-router-link.component';
import { PagedListComponent } from './shared-components/paged-list/paged-list.component';
import { UpdatePasswordDialogComponent } from './account/components/update-password-dialog/update-password-dialog.component';
import { ImageBrowserComponent } from './shared-components/image-browser/image-browser.component';
import { BlocDisplayComponent } from './blocs/bloc-display/bloc-display.component';

import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,

    LoginComponent,
    PasswordResetComponent,
    LoginDialogComponent,

    InfoComponent,
    ReadmeComponent,
    LegalNoticeComponent,

    AccountComponent,

    DeleteDialogComponent,
    LoadingComponent,
    ConditionalRouterLinkComponent,
    PagedGridComponent,
    PagedListComponent,
    UpdatePasswordDialogComponent,

    HallsDashComponent,
    WallsDashComponent,
    BlocsDashComponent,
    HoldsListComponent,
    EditBlocComponent,
    ImageBrowserComponent,
    EditWallComponent,
    BlocDisplayComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase), AngularFireAuthModule, AngularFireDatabaseModule, AngularFireStorageModule,
    MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatToolbarModule, MatSidenavModule, MatListModule, MatCardModule, MatSnackBarModule, MatSlideToggleModule, MatTabsModule, MatExpansionModule, MatCheckboxModule, MatPaginatorModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatDialogModule, MatProgressSpinnerModule, MatTableModule, MatSliderModule, MatTooltipModule,
    FlexLayoutModule,
    MarkdownModule.forRoot({
        markedOptions: {
          provide: MarkedOptions//,
          // useValue: {
          //   // FIXME: marked(): sanitize and sanitizer parameters are deprecated since version 0.7.0, should not be used and will be removed in the future.
          //   //        Read more here: https://marked.js.org/#/USING_ADVANCED.md#options marked.js:371
          //   //        Warning: This feature is deprecated and it should NOT be used as it cannot be considered secure.
          //   //        Instead use a sanitize library, like DOMPurify (recommended), sanitize-html or insane on the output HTML!
          //   sanitize: true
          // }
			}
    }),
	ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    AuthService, AdminService,
    DeviceSizeService, ToolsService,
    CanActivateAdmin, AccountGuard,
    DicoService, DatePipe,
    HallsService, WallsService, BlocsService, HoldsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
