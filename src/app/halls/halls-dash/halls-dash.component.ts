import { Component, OnInit } from '@angular/core';
import { HallsService, Hall } from '../halls.service';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-halls-dash',
  templateUrl: './halls-dash.component.html',
  styleUrls: ['./halls-dash.component.css']
})
export class HallsDashComponent implements OnInit {

	public hallsList: Hall[];
	private unsubscribe: Subject<void> = new Subject();

  constructor(
		public halls: HallsService
	) { }

  ngOnInit() {
		this.halls.getHalls()
		.pipe(takeUntil(this.unsubscribe))
		.subscribe((hallsList) => {
			this.hallsList = hallsList;
		});
  }

}
