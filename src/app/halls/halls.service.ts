import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, pipe } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { ToolsService } from '../providers/tools.service';

export class Hall {
  hallId: string;
  name: string;
	description: string;
  location: string;
  accessPlan: string;
}

@Injectable()
export class HallsService {

	private halls: Observable<Hall[]>;
	private hallsList: { [hallId: string]: Observable<Hall> } = {};

  constructor(
		private db: AngularFireDatabase,
		private tools: ToolsService
	) { }

	getHalls(): Observable<Hall[]> {
		if (!this.halls) {
			this.halls = this.tools.enableCache(
				this.db.list<Hall>('halls').valueChanges(),
				"halls"
			).pipe(
				shareReplay(1)
			);
		}
		return this.halls;
	}

	getHall(hallId: string): Observable<Hall> {
		if (!this.hallsList[hallId]) {
			this.hallsList[hallId] = this.getHalls().pipe(
				map(halls => halls.find(hall => hall.hallId == hallId))
			).pipe(
				shareReplay(1)
			);
		}
		return this.hallsList[hallId];
	}
}
