"""
A module dedicated to walls data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, NumberRules
from common import is_admin

class WallsRules(RulesPattern):
	"""
	Main node for walls rules
	"""
	def __build__(self):
		self.set_read("true")
		self.add(WallHallRules("$hallId"))

class WallHallRules(RulesPattern):
	"""
	Node walls/$hallId
	"""
	def __build__(self):
		self.set_write(is_admin(self.check_variable(self.label)))
		self.add(HallRules("$wallId"))

class HallRules(RulesPattern):
	"""
	Node walls/$hallId/$wallId
	"""
	def __build__(self):
		self.add(StringRules("wallId", 30))
		self.add(StringRules("name", 50))
		self.add(StringRules("image", 10000000))
		self.add(StringRules("date", 80))
		self.add(NumberRules("circleRadius"))
		self.add(NumberRules("circleWidth"))
		self.add(OtherRules())
