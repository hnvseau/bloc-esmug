"""
A module dedicated to admin data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, BooleanRules
from common import validate_uid

class AdminsRules(RulesPattern):
	"""
	Main node for admin rules
	"""
	def __build__(self):
		self.add(AdminRules("$uid"))

class AdminRules(RulesPattern):
	"""
	Node admin/$uid
	"""
	def __build__(self):
		self.set_read(validate_uid(self.check_variable(self.label)))
		self.add(NameRules("name"))
		self.add(HallsRules("halls"))
		self.add(BooleanRules("admin"))
		self.add(StringRules("email", 100))
		self.add(OtherRules())

class NameRules(RulesPattern):
	"""
	Node admin/$uid/name
	"""
	def __build__(self):
		self.add(StringRules("firstName", 30))
		self.add(StringRules("lastName", 30))
		self.add(OtherRules())

class HallsRules(RulesPattern):
	"""
	Node admin/$uid/halls
	"""
	def __build__(self):
		self.label = "halls"
		self.add(BooleanRules("$hallId"))
