"""
A module dedicated to halls data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules
from common import is_admin

class HallsRules(RulesPattern):
	"""
	Main node for halls rules
	"""
	def __build__(self):
		self.set_read("true")
		self.add(HallRules("$hallId"))

class HallRules(RulesPattern):
	"""
	Node halls/$hallId
	"""
	def __build__(self):
		self.set_write(is_admin(self.check_variable(self.label)))
		self.add(StringRules("hallId", 30))
		self.add(StringRules("name", 50))
		self.add(StringRules("description", 200))
		self.add(StringRules("location", 200))
		self.add(StringRules("accessPlan", 500))
		self.add(OtherRules())
