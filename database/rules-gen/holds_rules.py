"""
A module dedicated to holds data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, NumberRules
from frtdb_rules_generator.common import do_and
from common import is_member, is_admin

class HoldsRules(RulesPattern):
	"""
	Main node for holds rules
	"""
	def __build__(self):
		self.set_read("true")
		self.add(HoldsHallRules("$hallId"))

class HoldsHallRules(RulesPattern):
	"""
	Node holds/$hallId
	"""
	def __build__(self):
		self.set_write(is_admin(self.check_variable(self.label)))
		self.add(HoldsBlocsRules("blocs"))
		self.add(HoldsWallsRules("walls"))
		self.add(OtherRules())

# In a bloc
class HoldsBlocsRules(RulesPattern):
	"""
	Node holds/$hallId/blocs
	"""
	def __build__(self):
		self.add(HoldsBlocRules("$blocId"))

class HoldsBlocRules(RulesPattern):
	"""
	Node holds/$hallId/blocs/$blocId
	"""
	def __build__(self):
		self.set_write(do_and([is_member(), "!data.exists()"]))
		self.set_validate("root.child('blocs/' + $hallId + '/' + $blocId).exists()")
		self.add(WallListRules("$index"))

class WallListRules(RulesPattern):
	"""
	Node holds/$hallId/blocs/$blocId/$index
	"""
	def __build__(self):
		self.add(StringRules("$wallId", 30))
		self.add(NumberRules("index"))
		self.add(BlocHoldsRules("holds"))

class BlocHoldsRules(RulesPattern):
	"""
	Node holds/$hallId/blocs/$blocId/$index/holds
	"""
	def __build__(self):
		self.add(BlocHoldRules("$holdId"))

class BlocHoldRules(RulesPattern):
	"""
	Node holds/$hallId/blocs/$blocId/$index/holds/$holdId
	"""
	def __build__(self):
		self.add(StringRules("holdId", 30))
		self.add(NumberRules("type"))
		self.add(NumberRules("index"))
		self.add(NumberRules("xCoord"))
		self.add(NumberRules("yCoord"))
		self.add(OtherRules())

# On the wall
class HoldsWallsRules(RulesPattern):
	"""
	Node holds/walls
	"""
	def __build__(self):
		self.add(HoldsWallRules("$wallId"))

class HoldsWallRules(RulesPattern):
	"""
	Node holds/walls/$wallId
	"""
	def __build__(self):
		self.add(WallHoldsRules("$holdId"))

class WallHoldsRules(RulesPattern):
	"""
	Node holds/walls/$wallId/$holdId
	"""
	def __build__(self):
		self.add(StringRules("holdId", 30))
		self.add(NumberRules("xCoord"))
		self.add(NumberRules("yCoord"))
		self.add(OtherRules())
