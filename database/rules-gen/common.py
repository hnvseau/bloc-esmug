"""
Module for common functions in this project
"""
from frtdb_rules_generator.common import do_and


def is_member():
	"""
	Returns a condition that is true if a user can have access to public data
	in the app
	"""
	return "true"

def validate_uid(uid):
	"""
	Returns a condition that is true if the given uid is the same as
	the one of user identified
	"""
	return uid + " === auth.uid"

def is_admin(hall_id):
	"""
	Returns a condition that is true if the user identified is an admin for the
	given hall
	"""
	return do_and([
			"root.child('admins/' + auth.uid + '/halls/' + " + hall_id + ").val() == true",
			"root.child('admins/' + auth.uid + '/admin').val() == true",
	])
