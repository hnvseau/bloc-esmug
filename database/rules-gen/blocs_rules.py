"""
A module dedicated to blocs data rules
"""
from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules, StringRules, NumberRules, IdRules
from frtdb_rules_generator.common import do_and, do_or
from common import is_member, is_admin

class BlocsRules(RulesPattern):
	"""
	Main node for blocs rules
	"""
	def __build__(self):
		self.set_read("true")
		self.add(BlocHallRules("$hallId"))

class BlocHallRules(RulesPattern):
	"""
	Node blocs/$hallId
	"""
	def __build__(self):
		self.set_write(is_admin(self.check_variable(self.label)))
		self.add(BlocRules("$blocId", self.variables))

class BlocRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId
	"""
	def __build__(self):
		self.set_write(do_and([
				is_member(),
				"!data.exists()"
		]))
		self.set_validate("root.child('walls/' + " + self.check_variable("$hallId") + ").exists()")
		self.index_on = ["rate", "diff"]
		self.add(StringRules("blocId", 30))
		self.add(StringRules("description", 512))
		self.add(StringRules("author", 30))
		self.add(StringRules("date", 80))
		self.add(NumberRules("type"))
		self.add(RateRules("rate"))
		self.add(NbOfVotesRules("nbOfVotes"))
		self.add(DiffRules("diff"))
		self.add(DiffVotesRules("diffVotes"))
		self.add(DiffSumRules("diffSum"))
		self.add(WallIdListRules("wallIdList"))
		self.add(OtherRules())

class RateRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/rate
	"""
	def __build__(self):
		self.set_write(is_member())
		self.set_validate(do_and([
				"newData.isNumber()",
				do_or([
						"!data.exists()",
						do_and([
								"(newData.val() - data.val()) * data.parent().child('nbOfVotes').val() <= 5",
								"newData.parent().child('nbOfVotes').val() == data.parent().child('nbOfVotes').val() + 1"
						])
				])
		]))

class NbOfVotesRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/nbOfVotes
	"""
	def __build__(self):
		self.set_write(is_member())
		self.set_validate(do_and([
				"newData.isNumber()",
				"newData.parent().child('rate').exists()",
				do_or([
						"!data.exists()",
						"newData.val() == data.val() + 1"
				])
		]))

class DiffRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/diff
	"""
	def __build__(self):
		self.set_write(is_member())
		self.set_validate(do_and([
				"newData.isNumber()",
				do_or([
						"!data.exists()",
						do_and([
								# 3/4 des votes sont pour un changement de cotation
								do_or([
										"data.parent().child('diffSum').val() * 4 >= data.parent().child('diffVotes').val() * 3",
										"data.parent().child('diffSum').val() * 4 <= - data.parent().child('diffVotes').val() * 3"
								]),
								"data.parent().child('diffVotes').val() >= 10",
								# limite de variation
								do_or([
										"newData.val() == data.val() + 1",
										"newData.val() == data.val() - 1"
								]),
								# reset des votes si changement de valeur
								"newData.parent().child('diffVotes').val() == 0",
								"newData.parent().child('diffSum').val() == 0"
						])
				])
		]))

class DiffVotesRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/diffVotes
	"""
	def __build__(self):
		self.set_write(is_member())
		self.set_validate(do_and([
				"newData.isNumber()",
				do_or([
						do_and([
								"newData.val() == 0",
								"newData.parent().child('diffSum').val() == 0"
						]),
						"newData.val() == data.val() + 1"
				])
		]))

class DiffSumRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/diffSum
	"""
	def __build__(self):
		self.set_write(is_member())
		self.set_validate(do_and([
				"newData.isNumber()",
				do_or([
						do_and([
								"newData.val() == 0",
								"newData.parent().child('diffVotes').val() == 0"
						]),
						do_and([
								"newData.val() <= data.val() + 1",
								"newData.val() >= data.val() - 1"
						])
				]),
				"newData.parent().child('diffVotes').exists()"
		]))

class WallIdListRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/wallIdList
	"""
	def __build__(self):
		self.add(WallIdRules("$wallId"))

class WallIdRules(RulesPattern):
	"""
	Node blocs/$hallId/$blocId/wallIdList/$wallId
	"""
	def __build__(self):
		self.set_validate(do_and([
				StringRules.validate_string(30),
				IdRules.validate_identity(self.check_variable(self.label))
		]))
		self.set_inline(True)
