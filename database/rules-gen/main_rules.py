"""
Entry point for the app-bloc-esmug rules generation
"""

from frtdb_rules_generator.rules_pattern import RulesPattern
from frtdb_rules_generator.common import OtherRules
from halls_rules import HallsRules
from walls_rules import WallsRules
from blocs_rules import BlocsRules
from holds_rules import HoldsRules
from admins_rules import AdminsRules

class MainRules(RulesPattern):
	"""
	The top node
	"""

	def __build__(self):
		self.add(AdminsRules("admins"))
		self.add(HallsRules("halls"))
		self.add(WallsRules("walls"))
		self.add(BlocsRules("blocs"))
		self.add(HoldsRules("holds"))
		self.add(OtherRules())

if __name__ == "__main__":
	RULES = MainRules("rules")

	# Generate rules
	with open("database/database_rules.json", "w") as file:
		file.write(RULES.to_json())

	# Generate UML diagrams
	with open("database/database_archi.puml", "w") as file:
		file.write(RULES.to_plantuml())
